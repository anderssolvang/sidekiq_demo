# README

Simple Sidekiq demo, using redis, forman, guard-livereload and postgres.
* Install pg and redis.
* Download and bundle install
* rails db:create db:migrate
* foreman start -f Procfile.dev
* rails s

Find the provided csv file in root/csv-demo/csv-demo.csv
Then go to root and upload it. Refresh and Sidekiq has imported it.

Ruby version: 2.7
