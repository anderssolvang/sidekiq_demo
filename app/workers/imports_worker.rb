class ImportsWorker
  require 'csv'

  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(csv_path)
    CSV.foreach((csv_path), headers: false) do |import|
      User.create(first_name: import[1], last_name: import[2])
    end
  end
end
