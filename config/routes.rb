Rails.application.routes.draw do
  resources :imports
  resources :users
  root 'imports#index'

  # Sidekiq web view
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
end
